locals {
    rules = {
        ssh = {
            name = "ssh"
            priority = 101
            direction = "Inbound"
            access = "Allow"
            protocol = "Tcp"
            source_port_range = "*"
            destination_port_range = "22"
            source_address_prefixes = ["99.253.0.0/18"]
            destination_address_prefix = "*"
        }

        http = {
            name = "http"
            priority = 102
            direction = "Inbound"
            access = "Allow"
            protocol = "Tcp"
            source_port_range = "*"
            destination_port_range = "443"
            source_address_prefixes = ["99.253.0.0/18"]
            destination_address_prefix = "*"
        }

        httpBad = {
            name = "httpBad"
            priority = 103
            direction = "Inbound"
            access = "Allow"
            protocol = "Tcp"
            source_port_range = "*"
            destination_port_range = "80"
            source_address_prefixes = ["99.253.0.0/18"]
            destination_address_prefix = "*"
        }

        azureDevOpsHttp = {
            name = "azureDevOpsHttp"
            priority = 104
            direction = "Inbound"
            access = "Allow"
            protocol = "Tcp"
            source_port_range = "*"
            destination_port_range = "80"
            source_address_prefixes = ["0.0.0.0/0"]
            destination_address_prefix = "*"
        }

        azureDevOpsHttps = {
            name = "azureDevOpsHttps"
            priority = 105
            direction = "Inbound"
            access = "Allow"
            protocol = "Tcp"
            source_port_range = "*"
            destination_port_range = "443"
            source_address_prefixes = ["0.0.0.0/0"]
            destination_address_prefix = "*"
        }
    }
}