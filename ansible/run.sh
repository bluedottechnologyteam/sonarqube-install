#!/bin/bash
set -eux

#if [ ! -z "$BD_AWS_ID" ]; then
#    echo "Set the BD_AWS_ID environment variable before continuing"
#    exit 1
#fi

#if [ ! -v $BD_AWS_SECRET ]; then
#    echo "Set the BD_AWS_SECRET environment variable before continuing"
#    exit 1
#fi

export ANSIBLE_HOST_KEY_CHECKING=False 
ansible-playbook -i tf_ansible_inventory.yaml main.yaml