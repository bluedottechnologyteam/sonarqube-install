This repository contains the IaC for configuring the Azure environment to house an Ubuntu server and configure that Ubuntu server to run Sonarqube. The Azure environment building takes place using terraform. There are a couple of configurations needed to tighten access to the VM. The firewall configurations are located in terraform/nsgrules.tf. These rules should be reviewed and tightened as needed.

# Prerequisites

To execute the installation of Sonarqube, Python and Terraform are required on the operator's computer. 

# Azure configuration

Before we can configure the Azure environment, log into the environment on the CLI using the command:
```
az login
```
Once the login session has finished, ensure that you are using the SAST subscription.
```
az account set --subscription "SAST Subscription"
```

To execute the terraform deployment, first install terraform into your path on your desktop machine and run the following command:
```
cd terraform
terraform init
terraform plan
terraform apply 
```
You will have to run the second twice as the IP generation may take some time to resolve. This will create 2 files required for the ansible-playbook execution.

# VM configuration

An Ansible playbook is used to help install and configure the Ubuntu instance in Azure. To set up the environment to execute Ansible go into the ```ansible``` directory and run:
```
python3 -m venv .
source bin/activate
pip install -r requirements.txt
```
This will install all the packages required to run the Ansible playbook. There is a shell script created for convenience. Ensure that before running the shell script that you set the ```BD_AWS_ID``` and ```BD_AWS_SECRET``` environmental variables. Once the variables are set just execute the following command:
```
./run.sh
```
When the Ansible playbook has finished its execution, proceed over to Sonarqube's website https://sastbd.bluedotworks.ca. Click on 'Log in' located in the top right corner. The default credentials for Sonarqube are admin/admin.

After logging in, change the admin password by clicking the profile icon located on the top right and then 'My Account. Click on 'Security' and scroll down. There will be an entry for 'Old Password', 'New Password', and 'Confirm Password'. Create a new password for the admin account.

It is also recommended to force user logins to the Sonarqube environment to prevent unauthorized access. This can be done by clicking on the 'Administration' menu on the top menu bar. On the 'Administration' screen click on the 'Security' tab located near the bottom of the left pane. Scroll to the bottom and check 'Force user authentication' and click on 'Save' 

Create a new account to test the pipeline build. To create an account, click on the 'Administration' menu item on the top. In the Administration screen, click on the 'Security' menu option and click on 'Users'. On the 'Users' screen, click on the 'Create User' button located on the right side of the page.

Once the user has been created, log out of the Sonarqube environment

# Test run

Log into the Sonarqube environment with the newly created account. Once logged in, click on 'Create new project'. Enter in a Project key (e.g. ist) and then click 'Set Up'. Enter in the name for the token and click on 'Generate'. Copy the token that has been generated. Click on 'Continue to proceed'. Select the project's main language which will reveal the next step of instructions to follow. For the test run, the project is C#/VB.NET based. 

Since the project resides in Azure Pipelines, we will just make note of the executing commands.

To configure the Azure Pipeline to use Sonarqube, the following tasks need to be added to the top of the steps defined in the azure-pipelines.yaml file.
```
- task: Bash@3
    displayName: 'Fix SSL issue'
    inputs:
      targetType: 'filePath'
      filePath: 'sast.sh'
  - task: NodeTool@0
    inputs:
      versionSpec: 12.19.0
  - task: DotNetCoreCLI@2
    displayName: 'Install MSBuild Sonarscanner'
    inputs:
      command: 'custom'
      custom: 'tool'
      arguments: 'install --global dotnet-sonarscanner --version 4.8.0'
  - task: Bash@3
    displayName: 'Initiating sonarscanner'
    inputs:
      targetType: 'inline'
      script: 'export PATH=$HOME/.dotnet/tools:$PATH; dotnet-sonarscanner begin /k:"ist" /d:sonar.host.url="https://sastbd.bluedotworks.ca" /d:sonar.login="$(sonar_key)"'
    env:
      sonar_key: $(sonar_key)
  - task: DotNetCoreCLI@2
    displayName: 'Rebuilding dotnet project'
    inputs:
      command: 'build'
  - task: Bash@3
    displayName: 'Sending data to sonarqube'
    inputs:
      targetType: 'inline'
      script: 'export PATH=$HOME/.dotnet/tools:$PATH; export SONAR_SCANNER_OPTS="-Djavax.net.ssl.trustStore=/usr/lib/jvm/adoptopenjdk-8-hotspot-amd64/jre/lib/security/cacerts -Djavax.net.ssl.keyStore=/usr/lib/jvm/adoptopenjdk-8-hotspot-amd64/jre/lib/security/cacerts"; dotnet-sonarscanner end /d:sonar.login="$(sonar_key)"'
```
A helper script ```sast.sh``` needs to be added to the project repository. ```sast.sh``` needs to run to download the Let's Encrypt certificates and install them in Java's certificate keystores. The tasks help to configure the Azure DevOps VM to install the correct version of NodeJS and the dotnet sonarscanner packages. Once the packages are installed, the initialization process takes place between the Azure DevOps VM and the SonarQube VM. The .NET solution is then rebuilt so the sonarscanner can go through the source code to determine any vulnerabilities that exist. Once the build process is complete the results are uploaded to the Sonarqube server. The results can be viewed by logging into the SonarQube server and view the project. 

Tighter restrictions can be applied by making the projects private. This is done by going to 'Administration' from the top menu bar. Click on 'Projects' and then 'Management'. Click on the 'Settings' gear located on the right end of the project row and click on 'Edit Permissions' 

# Removal

In the event the SonarQube solution needs to be removed from the Azure environment, execute the following command in the terraform directory:
```
terraform destroy -auto-approve
```

# Vagrant
In the case that the operator is using a Windows machine. This solution can be deployed using Vagrant and VirtualBox. The ```Vagrantfile``` is located in the vagrant directory. Run the following instructions to create an  Ubuntu VM with the tools needed to run this solution.

```
cd vagrant
# git clone https://<username>@bitbucket.org/bluedottechnologyteam/sonarqube-install.git
vagrant up
vagrant ssh
```