provider "azurerm" {
    version = "=2.20.0"
    features {}
}

resource "random_password" "password" {
    length = 16
    special = true
    override_special = "_%@"
}

resource "azurerm_resource_group" "sast" {
    name        = "sast-rg"
    location    = "Canada East"
}

resource "azurerm_network_security_group" "sast-nsg" {
    name                = "sonarqube-sast-nsg"
    location            = azurerm_resource_group.sast.location
    resource_group_name = azurerm_resource_group.sast.name

    tags = {
        environment = "Dev"
        application = "SAST"
    }
}

resource "azurerm_network_security_rule" "sast-nsg-rules" {
        for_each    = local.rules
        name = each.key
        priority = each.value.priority
        direction = each.value.direction
        access = each.value.access
        protocol = each.value.protocol
        source_port_range = each.value.source_port_range
        destination_port_range = each.value.destination_port_range
        source_address_prefixes = each.value.source_address_prefixes
        destination_address_prefix = each.value.destination_address_prefix
        resource_group_name = azurerm_resource_group.sast.name
        network_security_group_name = azurerm_network_security_group.sast-nsg.name
}

resource "azurerm_virtual_network" "sast-vnet" {
    name                = "sast-vnet"
    location            = azurerm_resource_group.sast.location
    resource_group_name = azurerm_resource_group.sast.name
    address_space       = ["172.16.2.0/24"]
    dns_servers          = ["8.8.8.8","8.8.4.4"]

    tags = {
        environment = "Dev"
        application = "SAST"
    }
}

resource "azurerm_subnet" "sast-subnet" {
    name                    = "dev-subnet"
    resource_group_name     = azurerm_resource_group.sast.name
    virtual_network_name    = azurerm_virtual_network.sast-vnet.name
    address_prefixes        = ["172.16.2.0/24"]
}

resource "azurerm_network_interface" "sast-nic" {
    name                    = "sast-nic"
    location                = azurerm_resource_group.sast.location
    resource_group_name     = azurerm_resource_group.sast.name

    ip_configuration {
        name                            = "sast-ip-addr"
        subnet_id                       = azurerm_subnet.sast-subnet.id
        private_ip_address_allocation   = "Dynamic"
        public_ip_address_id = azurerm_public_ip.sast-public-ip.id
    }
}

resource "azurerm_network_interface_security_group_association" "sast-nic-nsg" {
    network_interface_id = azurerm_network_interface.sast-nic.id
    network_security_group_id = azurerm_network_security_group.sast-nsg.id
}

resource "azurerm_public_ip" "sast-public-ip" {
    name = "sast-public-ip"
    resource_group_name = azurerm_resource_group.sast.name
    location = azurerm_resource_group.sast.location
    allocation_method = "Dynamic"
    domain_name_label = "sast-bd"

    tags = {
        environment = "Dev"
        application = "SAST"
    }
}

resource "azurerm_virtual_machine" "sast-vm" {
    name = "sast-vm"
    location = azurerm_resource_group.sast.location
    resource_group_name = azurerm_resource_group.sast.name
    network_interface_ids = [azurerm_network_interface.sast-nic.id]
    vm_size = "Standard_DS1_v2"
    delete_os_disk_on_termination = true
    delete_data_disks_on_termination = true

    storage_image_reference {
        publisher = "Canonical"
        offer = "UbuntuServer"
        sku = "18.04-LTS"
        version = "latest"
    }
    storage_os_disk {
        name = "sast-os-disk1"
        caching = "ReadWrite"
        create_option = "FromImage"
        managed_disk_type = "Standard_LRS"
    }
    os_profile {
        computer_name = "sonarqube"
        admin_username = "adminroot"
        admin_password = random_password.password.result
    }
    os_profile_linux_config {
        disable_password_authentication = false
    }
    tags = {
        environment = "Dev"
        application = "SAST"
    }
}