output "adminPassword" {
    sensitive = false
    value = random_password.password.result
}

output "sonarqubeIP" {
    value = azurerm_public_ip.sast-public-ip.ip_address
}

resource "local_file" "tf_ansible_inventory" {
    content = <<-VARS
    all:
        hosts:
            sonarqube:
                ansible_host: ${azurerm_public_ip.sast-public-ip.ip_address}
                ansible_user: adminroot
                ansible_password: ${random_password.password.result}
    VARS
    filename = "../ansible/tf_ansible_inventory.yaml"
}

resource "local_file" "ansible_all" {
    content = <<-VARS
    hostIP: ${azurerm_public_ip.sast-public-ip.ip_address}
    VARS
    filename = "../ansible/group_vars/all"
}